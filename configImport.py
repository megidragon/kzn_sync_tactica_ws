import yaml
import sys
import os
if getattr(sys, 'frozen', False):
    # frozen
    dir_ = os.path.dirname(sys.executable)
else:
    # unfrozen
    dir_ = os.path.dirname(os.path.realpath(__file__))
__location__ = os.path.realpath(os.path.join(os.getcwd(), dir_))+os.sep

config = {}
with open(__location__+"config.yml", 'r') as stream:
    try:
        config = yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(-1)