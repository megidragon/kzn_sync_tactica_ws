# -*- coding: utf-8 -*-
print('V1.0.1')
import os
import sys
from sys import exit
from database import DataBase
from mainClass import Main
from configImport import config, __location__
from multiprocessing import Queue
import zipfile
import shutil
from sys import exit
import datetime
import time
import socket
import requests

###### INIT VARIABLES ######
LOG = os.path.join(__location__, 'log', "log.log")


def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


##### INIT END ######

def check_if_server_is_up():
    try:
        response = requests.get(config['WS_HOST']+':'+str(config['WS_PORT']), timeout=5)
        return True
    except:
        return False

def main():
    database = DataBase(
        config['DB_HOST'],
        config['DB_PORT'],
        config['DB_USER'],
        config['DB_PASSWORD'],
        config['DB_DATABASE']
    )

    main = Main(database, config)
    while True:
        if not check_if_server_is_up():
            print('Server down...')
            if not config['INFINITE_LOOP']:
                break
            time.sleep(config['UPDATE_INTERVAL'])

        main.process()
        if not config['INFINITE_LOOP']:
            break

        time.sleep(config['UPDATE_INTERVAL'])

def init():
    database = DataBase(
        config['DB_HOST'],
        config['DB_PORT'],
        config['DB_USER'],
        config['DB_PASSWORD'],
        config['DB_DATABASE']
    )
    database.connect()
    main = Main(database, config)
    info = main.getInfo()
    main.installProgram(database, info)
    print('Se finalizo la instalacion')
    exit()

if __name__== "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'init':
        init()
    else:
        main()