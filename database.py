# -*- coding: utf-8 -*-
import pymysql
import datetime


class DataBase:
    db = False
    error = False

    def __init__(self, DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_DATABASE):
        self.DB_HOST = DB_HOST
        self.DB_PORT = DB_PORT
        self.DB_USER = DB_USER
        self.DB_PASSWORD = DB_PASSWORD
        self.DB_DATABASE = DB_DATABASE

    def query(self, query, parse_datetime=False):
        cursor = self.db.cursor()
        cursor.execute(query.encode('utf-8'))
        returnData = cursor.fetchall()
        cursor.close()

        if parse_datetime:
            for i in range(0, len(returnData)):
                for k, x in returnData[i].items():
                    returnData[i][k] = str(x) if isinstance(x, datetime.date) else x

        return returnData

    def execute(self, query):
        #try:
        cursor = self.db.cursor()
        cursor.execute(query)
        cursor.close()
        return True
        #except:
        #    print('Failed insert')
        #    return False


    def connect(self):
        self.db = pymysql.connect(
            host =   str(self.DB_HOST),
            port =   int(self.DB_PORT),
            user =   str(self.DB_USER),
            passwd = self.DB_PASSWORD,
            db =     str(self.DB_DATABASE),
            cursorclass=pymysql.cursors.DictCursor,
            autocommit=True,
            charset='utf8'
        )

    def test_connection(self):
        try:
            self.connect()
            self.closeConnection()
            return True
        except Exception as e:
            self.error = e
            return False

    def closeConnection(self):
        self.db.close()