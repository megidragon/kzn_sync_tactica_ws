# -*- coding: utf-8 -*-
from itertools import islice
import json
import requests
from sys import exit
from configImport import config, __location__
import logging
import re

logging.basicConfig(level=logging.INFO, filename=__location__+'log.dat', format='%(asctime)s :: %(levelname)s :: %(message)s')

def printe(t):
    print(t)
    exit()

def log(text, type='info'):
    if (type == 'info'):
        logging.info(text)
    elif (type == 'warning'):
        logging.warning(text)
    elif (type == 'error'):
        logging.error(text)
    elif (type == 'critical'):
        logging.critical(text)

    if config['DEBUG_MODE']:
        print(text)


class Main:
    allowedModules = []
    def __init__(self, database, config):
        self.database = database
        self.config = config
        self.url = self.config['WS_HOST']+':'+str(self.config['WS_PORT'])+self.config['WS_PREFIX']+'sync'
    # END FUNCTION

    def process(self):
        log("Verificando estado...")
        info = self.getInfo()

        if 'service_status' not in info or info['service_status'] != True:
            log('Error al traer la informacion', type='critical')
            log(info, type='critical')
            return False

        # Si no valida el campo sync installed ejecuta el init.
        self.database.connect()
        if info['sync_installed'] == False:
            self.installProgram(self.database, info)


        requestJson = self.generateRequest(info)

        if len(requestJson['modules']) > 0:
            if self.sync(requestJson, first_sync=not info['sync_installed'], info=info):
                self.clearAuditTable()
                return True
            else:
                log("Sin modulos...")
        else:
            log('Estado sincronizado.')
        # END IF
        return False
    # END FUNCTION

    def sync(self, jsonData, first_sync=False, info=False):
        """
        Tenemos X modulos con diferentes tablas, relaciones y columnas, y pasaremos diferente informacion segun el modo, entonces como hacemos?
        lo q necesitamos definir es:
        Nombre de tabla
        nombre clave
        columnas que necesita el WS
        una query para traer toda la info que necesita
        :return:
        """
        url = self.url
        if first_sync:
            log('Primera sincronizacion')
            subJson = {'modules': {}}
            i = 1
            modulesLen = len(info['modules'])
            for table in info['order']:
                print('\r\n\r\n--------------------------------------------')
                log('Sincronizando modulo {}'.format(table))
                print('--------------------------------------------')
                rows = jsonData['modules'][table]
                newRows = self.chunks(rows, config['CHUNK_SIZE'])
                newRowsLen = len(newRows)
                i2 = 1
                for row in newRows:
                    if i2 == newRowsLen and i == modulesLen:
                        log('Enviando ultima request')
                        subJson['sync_installed'] = True
                    subJson['modules'][table] = row
                    print("\r\n--------------------------------------------")
                    log('Enviando {0} registros al servidor...'.format(len(subJson['modules'][table])))

                    response = self.request(url, 'post', subJson)

                    if response.status_code == 200:
                        if not response.json()['success']:
                            log("Error al ejecutar la request", type='error')
                            log(response.json()['error']['msg'], type='error')
                            break
                        # END IF
                        log('REQUEST FINALIZADA CON EXITO')
                        print("--------------------------------------------")
                    elif response.status_code == 500:
                        try:
                            log(response.json()['error']['msg'], type='error')
                            log('ERROR AL CORRER LA REQUEST', type='error')
                            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                        except:
                            log('ERROR INTERNO DEL SERVICIO.', type='critical')
                            log(subJson, type='error')
                            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

                        break
                    else:
                        log('ERROR DESCONOCIDO AL CORRER LA REQUEST', type='error')
                        log(subJson, type='error')
                        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

                        break
                    # END IF

                    i2 += 1
                # END FOR
                i += 1
            # END FOR
        else:
            response = self.request(url, 'post', jsonData)
            if response.status_code == 200 and response.json()['success']:
                log('REQUEST FINALIZADA CON EXITO')
                print(response.content)
                return response.json()['success']
            elif response.status_code == 500:
                try:
                    log('ERROR AL CORRER LA REQUEST', type='error')
                    log(response.json()['error']['msg'])
                except:
                    log('ERROR INTERNO AL CORRER LA REQUEST', type='critical')
            else:
                log('ERROR AL CORRER LA REQUEST, codigo de error no capturado', type='error')
            # END IF
        # END IF
    # END FUNCTION

    def chunks(self, l, n):
        n = max(1, n)
        return [l[x:x+n] for x in range(0, len(l), n)]
    # END FUNCTION

    def getInfo(self):
        url = self.url
        return self.request(url, 'get').json()
    # END FUNCTION

    def syncFirstTime(self):
        pass
    # END FUNCTION

    def request(self, url, method, payload=None):
        if payload is None:
            payload = {}

        self.parseRquest(payload=payload)
        headers = {"AuthorizationBusiness": self.config['WS_ACCESS_TOKEN']}

        if method == 'get':
            return requests.get(url, headers=headers)
        elif method == 'post':
            return requests.post(url, json=payload, headers=headers)
        elif method == 'put':
            pass
        elif method == 'delete':
            pass
        else:
            Exception('Methond not defined')
    # END FUNCTION

    def parseRquest(self, payload):
        type = 'dict' if isinstance(payload, dict) else 'list'
        resp = {} if type=='dict' else []
        for i in payload:
            pl = payload[i] if isinstance(payload, dict) else i
            if isinstance(pl, str):
                pl = pl.replace('\\', '')
            elif isinstance(pl, dict) or isinstance(pl, list):
                pl = self.parseRquest(pl)

            if type == 'dict':
                resp[i] = pl
            else:
                resp.append(pl)
        return resp

    def generateRequest(self, info):
        self.allowedModules = info['modules']
        moduleFormat = info['modules_process_format']

        audit = self.database.query("SELECT * FROM auditkaizen2b WHERE tactica_id IS NOT NULL AND tactica_id != '';")
        inserts = {}
        updates = {}
        deletes = {}

        requestJson = {'modules': {}}

        if len(audit) > 0 or info['sync_installed'] != True:
            if info['sync_installed']:
                tables = list(dict.fromkeys([row['table_name'] for row in audit]))
                for row in audit:
                    if row['table_name'] not in moduleFormat:
                        log('invalid module for table {0}'.format(row['table_name']), type='warning')
                        continue

                    if row['operation_type'] == 'insert':
                        # junta los id de tacticaque se encontraron
                        if row['table_name'] not in inserts:
                            inserts[row['table_name']] = []

                        inserts[row['table_name']].append(row['tactica_id'])

                    elif row['operation_type'] == 'update':
                        if row['table_name'] not in updates:
                            updates[row['table_name']] = []

                        updates[row['table_name']].append(row['tactica_id'])

                    elif row['operation_type'] == 'delete':
                        if row['table_name'] not in deletes:
                            deletes[row['table_name']] = []

                        deletes[row['table_name']].append(row['tactica_id'])

                    else:
                        log('Undefined operation_type in id: {0}'.format(row['id']), type='error')
                # END FOR
            else:
                tables = list(dict.fromkeys([key for key in moduleFormat]))
            # END IF

            for table in tables:
                serverQuery = moduleFormat[table]['sql']
                if serverQuery.find('WHERE') == -1:
                    serverQuery = serverQuery + ' WHERE 1 '

                # Para los inserts
                if table in inserts or not info['sync_installed']:
                    if info['sync_installed']:
                        tactica_ids = ''
                        insertsLen = len(inserts[table])
                        for x in range(0, len(inserts[table])):
                            tactica_ids += '"' + str(inserts[table][x]) + '"'
                            tactica_ids += ', ' if x < insertsLen - 1 else ''
                        # END FOR

                        sql = serverQuery + " AND {table_name}.RecID IN {tactica_id}".format(
                            table_name=table,
                            tactica_id='(' + tactica_ids + ')'
                        )
                        queryResults = self.database.query(sql, parse_datetime=True)
                    else:
                        sql = serverQuery
                        queryResults = self.database.query(sql, parse_datetime=True)
                    # END IF

                    for row in queryResults:
                        if moduleFormat[table]['ws_table_name'] not in requestJson['modules']:
                            requestJson['modules'][moduleFormat[table]['ws_table_name']] = []
                        # END IF
                        requestJson['modules'][moduleFormat[table]['ws_table_name']].append({
                            'operation_type': 'insert',
                            'data': row
                        })
                    # END FOR
                # END IF

                # Si es la primera vez q sincroniza solo inserta todos los datos
                if not info['sync_installed']:
                    continue

                # Para los updates
                if table in updates:
                    tactica_ids = ''
                    updatesLen = len(updates[table])
                    for x in range(0, len(updates[table])):
                        tactica_ids += '"' + str(updates[table][x]) + '"'
                        tactica_ids += ', ' if x < updatesLen - 1 else ''

                    sql = serverQuery + " AND {table_name}.RecID IN {tactica_id}".format(
                        table_name=table,
                        tactica_id='(' + tactica_ids + ')'
                    )

                    queryResults = self.database.query(sql, parse_datetime=True)

                    for row in queryResults:
                        if moduleFormat[table]['ws_table_name'] not in requestJson['modules']:
                            requestJson['modules'][moduleFormat[table]['ws_table_name']] = []
                        # END IF
                        requestJson['modules'][moduleFormat[table]['ws_table_name']].append({
                            'operation_type': 'update',
                            'data': row
                        })
                    # END FOR
                # END IF

                # Para los deletes
                if table in deletes:
                    tactica_ids = ''
                    deletesLen = len(deletes[table])
                    for x in range(0, len(deletes[table])):
                        tactica_ids += '"' + str(deletes[table][x]) + '"'
                        tactica_ids += ', ' if x < deletesLen - 1 else ''
                    # END FOR

                    sql = serverQuery + " AND {table_name}.RecID IN {tactica_id}".format(
                        table_name=table,
                        tactica_id='(' + tactica_ids + ')'
                    )

                    queryResults = self.database.query(sql, parse_datetime=True)

                    for row in queryResults:
                        if moduleFormat[table]['ws_table_name'] not in requestJson['modules']:
                            requestJson['modules'][moduleFormat[table]['ws_table_name']] = []
                        # END IF
                        requestJson['modules'][moduleFormat[table]['ws_table_name']].append({
                            'operation_type': 'delete',
                            'data': {'tactica_id': row['tactica_id']}
                        })
                    # END FOR
                # END IF
        # END IF
        return requestJson
    # END FUNCTION

    def clearAuditTable(self):
        self.database.execute("DELETE FROM auditkaizen2b")

    def installProgram(self, database, info):
        database.execute("DROP TABLE IF EXISTS auditkaizen2b")
        database.execute("""
                CREATE TABLE `auditkaizen2b` (
                  `id` INT NOT NULL AUTO_INCREMENT,
                  `operation_type` VARCHAR(45) NOT NULL,
                  `tactica_id` VARCHAR(45) NOT NULL,
                  `table_name` VARCHAR(45) NOT NULL,
                  `created_at` DATETIME NOT NULL,
                  PRIMARY KEY (`id`));
              """)

        if 'service_status' not in info or info['service_status'] != True:
            log('Error al traer la informacion', 'critical')
            log(info)
            return False
        log('Corriendo triggers')
        self.generateTriggers(database, info['modules_process_format'])

    def generateTriggers(self, database, tables):
        updateSql = """
        CREATE DEFINER = CURRENT_USER TRIGGER `{table_name}_BEFORE_UPDATE` BEFORE UPDATE ON `{table_name}` FOR EACH ROW
            BEGIN
                INSERT INTO auditkaizen2b
                SET operation_type = 'update',
                 tactica_id = OLD.RecID,
                    table_name = '{table_name}',
                    created_at = NOW(); 
            END;
        """

        insertSql = """
            CREATE DEFINER = CURRENT_USER TRIGGER `{table_name}_BEFORE_INSERT` BEFORE INSERT ON `{table_name}` FOR EACH ROW
                BEGIN
                    INSERT INTO auditkaizen2b
                    SET operation_type = 'insert',
                     tactica_id = NEW.RecID,
                        table_name = '{table_name}',
                        created_at = NOW(); 
                END;
            """

        deleteSql = """
            CREATE DEFINER = CURRENT_USER TRIGGER `{table_name}_BEFORE_DELETE` BEFORE DELETE ON `{table_name}` FOR EACH ROW
                BEGIN
                    INSERT INTO auditkaizen2b
                    SET operation_type = 'delete',
                     tactica_id = OLD.RecID,
                        table_name = '{table_name}',
                        created_at = NOW(); 
                END;
            """

        for key, table in tables.items():
            if len(database.query("SHOW TRIGGERS LIKE '{table_name}'".format(table_name=key))):
                database.execute("DROP TRIGGER {table_name}_BEFORE_UPDATE".format(table_name=key))
                database.execute("DROP TRIGGER {table_name}_BEFORE_INSERT".format(table_name=key))
                database.execute("DROP TRIGGER {table_name}_BEFORE_DELETE".format(table_name=key))

            database.execute(updateSql.format(table_name=key))

            database.execute(insertSql.format(table_name=key))

            database.execute(deleteSql.format(table_name=key))